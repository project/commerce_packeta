(function ($, Drupal) {
  Drupal.behaviors.packetaWidget = {
    attach: function (context, settings) {

      $('.packeta-selector-open').on('click', function () {

        let appIdentity = drupalSettings.version;

        let options = {
          webUrl: drupalSettings.path.baseUrl,
          country: this.dataset.country,
          language: drupalSettings.language,
          appIdentity: appIdentity,
          fontFamily: drupalSettings.packetaWidgetFont,
        }

        Packeta.Widget.pick(drupalSettings.packetaApiKey, function (pickupPoint) {
          if (pickupPoint != null) {
            $('.packeta-selected-point-id').val(pickupPoint.id);

            $('.packeta-selected-point-name .point-name-value').text(pickupPoint.name);

            $('.packeta-selected-point-country-code').val(pickupPoint.country);
            $('.packeta-selected-point-locality').val(pickupPoint.city);
            $('.packeta-selected-point-postal-code').val(pickupPoint.zip);
            $('.packeta-selected-point-address-line1').val(pickupPoint.street);
            $('.packeta-selected-point-organization').val(pickupPoint.place);
          }
        }, options);
      });
    }
  }
}(jQuery, Drupal));
