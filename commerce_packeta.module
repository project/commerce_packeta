<?php

use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Implements hook_entity_view().
 */
function commerce_packeta_profile_view(array &$build, ProfileInterface $profile, EntityViewDisplayInterface $display, $view_mode) {
  if ($pickup_point_id = $profile->getData('pickup_point_id')) {
    if (Drupal::service('router.admin_context')->isAdminRoute()) {
      $build['pickup_point'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => t('Packeta pickup point ID: @id', ['@id' => $pickup_point_id]),
      ];
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_presave().
 */
function commerce_packeta_commerce_shipment_presave(ShipmentInterface $shipment) {
  $original_state = $shipment->getState()->getOriginalId();
  $new_state = $shipment->getState()->getId();

  // Make sure that state is changing
  if ($original_state == 'draft' && $original_state != $new_state) {
    $plugin_id = $shipment->getShippingMethod()->getPlugin()->getPluginId();

    if ($plugin_id == 'packeta' && $shipment->getTrackingCode() == NULL) {

      /** @var \Drupal\commerce_packeta\PacketaApiClientInterface $packetaApiClient */
      $packetaApiClient = Drupal::service('commerce_packeta.packeta_api_client');

      $tracking_code = $packetaApiClient->submitShipment($shipment);

      if (!empty($tracking_code)) {
        $shipment->setTrackingCode($tracking_code);
      } else {

        $request = Drupal::request();

        $response = new \Symfony\Component\HttpFoundation\RedirectResponse($request->getUri());
        $response->send();

        Drupal::messenger()->addError(t('Failed to retrieve tracking code from Packeta API. Check logs for more details.'));

        $shipment->state->setValue($original_state);
      }
    }
  }

}
