# COMMERCE PACKETA

Provides Packeta shipping widget for Drupal Commerce

## SETUP

1. Enable module.

2. Go to `/admin/commerce/config/checkout-flows`:

* Edit checkout flow where you want to use Packeta
* Replace **Shipping Information** with **Shipping Information compatible with Packeta**
* Save

### Important notice
Since 2021-09-01 packeta API requires weight to successfully submit a package.

We included submodule Commerce Packeta Views (commerce_packeta_views),
that creates a view which allows you to bulk edit product variations and set their weight if it's enabled.
