# COMMERCE PACKETA VIEWS

Provides view with bulk edit for product variations

Go to `/admin/commerce/product-variations`:

* **Optional:** Edit this view and add fields, filters or sorts if needed
* Use it to set weight for multiple product variations at once
