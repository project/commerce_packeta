<?php

namespace Drupal\commerce_packeta;

use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Interface of the pickup mapper.
 */
interface PickupProfileMapperInterface {

  /**
   * Populates the profile.
   *
   * @param ProfileInterface $profile
   *   The profile to populate.
   * @param array $point_data
   *   Pickup point data
   */
  public function populateProfile(ProfileInterface $profile, array $point_data): void;

  /**
   * Sets current shipping method
   *
   * @param ShippingMethodInterface $shippingMethod
   *   Current shipping method
   */
  public function setShippingMethod(ShippingMethodInterface $shippingMethod): void;

  /**
   * Defines the type of input field the pane should provide.
   *
   * @param ProfileInterface $profile
   *   The profile as context.
   */
  public function buildFormElement(ProfileInterface $profile): array;

}
