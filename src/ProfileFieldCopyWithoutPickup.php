<?php

namespace Drupal\commerce_packeta;

use Drupal\commerce_shipping\ProfileFieldCopy;
use Drupal\commerce_packeta\Plugin\Commerce\CheckoutPane\PickupCapableShippingInformation;
use Drupal\Core\Form\FormStateInterface;

/**
 * If the a pickup method is selected, disable billing same as shipping.
 */
final class ProfileFieldCopyWithoutPickup extends ProfileFieldCopy {

  /**
   * {@inheritdoc}
   */
  public function supportsForm(array &$inline_form, FormStateInterface $form_state) {
    $parent = parent::supportsForm($inline_form, $form_state);

    $order = self::getOrder($form_state);
    $complete_form = $form_state->getCompleteForm();

    if (empty($complete_form['packeta_capable_shipping_information'])) {

      // The case of form being loaded and pickup method already saved in order
      if (PickupCapableShippingInformation::isShipmentPickup($order)) {
        return FALSE;
      }

      return $parent;
    }

    $form = $complete_form['packeta_capable_shipping_information'];

    if (PickupCapableShippingInformation::isPickupSelected($form, $form_state, $order)) {
      return FALSE;
    }

    return $parent;
  }

  public static function submitForm(array &$inline_form, FormStateInterface $form_state) {
    $order = self::getOrder($form_state);
    $form = $form_state->getCompleteForm()['packeta_capable_shipping_information'];

    if (!$form) {
      parent::submitForm($inline_form, $form_state);
      return;
    }

    if (PickupCapableShippingInformation::isPickupSelected($form, $form_state, $order)) {
      $form_state->setError($inline_form, t('Can not use "Billing same as shipping while choosing this shipping method"'));
    }

    parent::submitForm($inline_form, $form_state);
  }

}
