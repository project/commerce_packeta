<?php


namespace Drupal\commerce_packeta;


use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use SoapClient;
use SoapFault;

class PacketaApiClient implements PacketaApiClientInterface {

  use StringTranslationTrait;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  public function __construct(LoggerChannelFactoryInterface $logger_factory) {
    $this->logger = $logger_factory->get(self::LOGGER_CHANNEL);
  }

  /**
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return string
   *    Tracking code
   */
  public function submitShipment(ShipmentInterface $shipment): string {

    $order = $shipment->getOrder();

    // Use billing profile to retrieve customer information
    $billingProfile = $order->getBillingProfile();
    $address = $billingProfile->get('address')->getValue()[0];

    // Retrieve order price for insurance purposes
    $totalPrice = $order->getTotalPrice();
    $shippingPrice = $shipment->getAmount();
    $orderPrice = $totalPrice->subtract($shippingPrice);

    $configuration = $shipment->getShippingMethod()
      ->getPlugin()
      ->getConfiguration();

    $cash_on_delivery = $configuration['settings']['cash_on_delivery'];
    $pickup_point_id = $shipment->getShippingProfile()
      ->getData('pickup_point_id');

    $telephone_field = $configuration['settings']['telephone_field'];
    if ($telephone_field && $billingProfile->hasField($telephone_field)) {
      $phone_number = $billingProfile->get($telephone_field)->getString();
    }

    $weight = 0;

    if ($shipment_weight = $shipment->getWeight()) {
      $weight = $shipment_weight->convert('kg')->getNumber();
    }

    $packet_array = [
      'number' => $order->getOrderNumber(),
      'name' => $address['given_name'],
      'surname' => $address['family_name'],
      'email' => $order->getEmail(),
      'currency' => $totalPrice->getCurrencyCode(),
      'value' => $orderPrice->getNumber(),
      'addressId' => $pickup_point_id,
      'eshop' => $configuration['settings']['eshop_url'],
      'weight' => $weight,
    ];

    if ($cash_on_delivery) {
      $packet_array['cod'] = $totalPrice->getNumber();
    }

    if (!empty($phone_number)) {
      $packet_array['phone'] = $phone_number;
    }

    try {
      $gw = new SoapClient("https://www.zasilkovna.cz/api/soap.wsdl");
      $api_password = $configuration['api']['api_password'];

      $packet = $gw->createPacket($api_password, $packet_array);

      return $packet->id;
    } catch (SoapFault $e) {
      $fault = $e->faultstring;
      $detail = serialize($e->detail);
      $this->logger->error($this->t('Error during submitting Shipment to Packeta API. Fault: @fault, detail: @detail', [
        '@fault' => $fault,
        '@detail' => $detail,
      ]));
    }

    return '';
  }

}
