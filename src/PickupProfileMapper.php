<?php

namespace Drupal\commerce_packeta;

use Drupal;
use Drupal\commerce_packeta\Plugin\Commerce\ShippingMethod\Packeta;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\Core\Extension\ExtensionList;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides packeta shipping profile form element
 */
class PickupProfileMapper implements PickupProfileMapperInterface {

  protected $languageManager;

  protected $extensionList;

  public function __construct(LanguageManagerInterface $languageManager, ExtensionList $extensionList) {
    $this->languageManager = $languageManager;
    $this->extensionList = $extensionList;
  }

  use StringTranslationTrait;

  /**
   * @var ShippingMethodInterface $shippingMethod
   */
  protected $shippingMethod;

  /**
   * {@inheritdoc}
   */
  public function populateProfile(ProfileInterface $profile, $point_data): void {
    unset($point_data['id']);
    unset($point_data['name']);

    $point_data['country_code'] = strtoupper($point_data['country_code']);

    $profile->set('address', $point_data);
  }

  /**
   * {@inheritdoc}
   */
  public function setShippingMethod(ShippingMethodInterface $shippingMethod): void {
    $this->shippingMethod = $shippingMethod;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(ProfileInterface $profile): array {
    $configuration = $this->shippingMethod->getPlugin()->getConfiguration();

    $current_language = $this->languageManager->getCurrentLanguage()->getId();

    $openButtonSelector = $configuration['widget']['widget_selector_open'];
    $selectors = explode(' ', $openButtonSelector);

    $selector_classes = [];

    foreach ($selectors as $selector) {
      if (strpos($selector, '.') === 0) {
        $selector = str_replace('.', '', $selector);
        $selector_classes[] = $selector;
      }
    }

    $selectors = implode(' ', $selector_classes);

    $allowed_countries = array_filter($configuration['settings']['allowed_countries'], function ($item) {
      return $item !== 0;
    });

    if (empty($allowed_countries)) {
      $allowed_countries = $configuration['settings']['allowed_countries'];
    }

    if (count($allowed_countries) == 1) {
      $countries_widget = [
        '#type' => 'item',
        '#title' => $this->t('Pickup point'),
      ];

      $country_code = reset($allowed_countries);

      $countries_widget[$country_code] = [
        '#type' => 'item',
        '#markup' => Markup::create(
          '<a href="#" class="packeta-selector-open ' . $selectors . '" data-country="' . $country_code . '">' . $this->t('Choose') . '</a>'
        ),
      ];
    }
    else {
      $countries_widget = [
        '#type' => 'item',
        '#title' => $this->t('Choose pickup country'),
      ];

      foreach ($allowed_countries as $country_code => $country) {
        $country = Packeta::AVAILABLE_COUNTRIES[$country_code];
        $countries_widget[$country_code] = [
          '#type' => 'item',
          '#markup' => Markup::create(
            '<a href="#" class="packeta-selector-open ' . $selectors . '" data-country="' . $country_code . '">' . $this->t($country) . '</a>'
          ),
        ];
      }
    }

    $module_info = $this->extensionList->getExtensionInfo('commerce_packeta');

    $drupal_version = Drupal::VERSION;
    $module_version = $module_info['version'];

    $version = "drupal-$drupal_version-commerce_packeta-$module_version";

    $default_values = $this->getDefaultValues($profile);

    if (isset($default_values['address_line1']) && isset($default_values['locality'])) {
      $point_name = $default_values['locality'] . ', ' . $default_values['address_line1'];
    }
    else {
      $point_name = $this->t('None');
    }

    return [
      '#type' => 'container',
      '#attached' => [
        'library' => [
          'commerce_packeta/packeta',
        ],
        'drupalSettings' => [
          'language' => $current_language,
          'packetaApiKey' => $configuration['api']['api_key'],
          'packetaWidgetFont' => $configuration['widget']['widget_font'],
          'version' => $version,
        ],
      ],
      '#attributes' => [
        'class' => ['packeta-widget-wrapper'],
      ],
      'widget' => $countries_widget,
      'selected_point' => [
        '#type' => 'container',
        'name' => [
          '#type' => 'item',
          '#title' => $this->t('Selected point'),
          '#markup' => '<div class="point-name-value">' . $point_name . '</div>',
          '#prefix' => '<div class="packeta-selected-point-name">',
          '#suffix' => '</div>',
        ],
        'id' => [
          '#type' => 'hidden',
          '#default_value' => isset($default_values['id']) ? $default_values['id'] : "",
          '#attributes' => [
            'class' => ['packeta-selected-point-id'],
          ],
        ],
        'country_code' => [
          '#type' => 'hidden',
          '#default_value' => isset($default_values['country_code']) ? $default_values['country_code'] : "",
          '#attributes' => [
            'class' => ['packeta-selected-point-country-code'],
          ],
        ],
        'locality' => [
          '#type' => 'hidden',
          '#default_value' => isset($default_values['locality']) ? $default_values['locality'] : "",
          '#attributes' => [
            'class' => ['packeta-selected-point-locality'],
          ],
        ],
        'postal_code' => [
          '#type' => 'hidden',
          '#default_value' => isset($default_values['postal_code']) ? $default_values['postal_code'] : "",
          '#attributes' => [
            'class' => ['packeta-selected-point-postal-code'],
          ],
        ],
        'address_line1' => [
          '#type' => 'hidden',
          '#default_value' => isset($default_values['address_line1']) ? $default_values['address_line1'] : "",
          '#attributes' => [
            'class' => ['packeta-selected-point-address-line1'],
          ],
        ],
        'organization' => [
          '#type' => 'hidden',
          '#default_value' => isset($default_values['organization']) ? $default_values['organization'] : "",
          '#attributes' => [
            'class' => ['packeta-selected-point-organization'],
          ],
        ],
      ],
    ];
  }

  protected function getDefaultValues(ProfileInterface $profile): array {
    if (!$profile->get('address')->isEmpty()) {
      $values = [];

      $values['id'] = $profile->getData('pickup_point_id');

      $address = $profile->get('address')->first()->getValue();

      return array_merge($values, $address);
    }
    return [];
  }

}
