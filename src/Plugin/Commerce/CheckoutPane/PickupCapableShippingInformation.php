<?php

namespace Drupal\commerce_packeta\Plugin\Commerce\CheckoutPane;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\OrderShipmentSummaryInterface;
use Drupal\commerce_shipping\PackerManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\CheckoutPane\ShippingInformation;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides the shipping information pane.
 *
 * Collects the shipping profile, then the information for each shipment.
 * Assumes that all shipments share the same shipping profile.
 *
 * @CommerceCheckoutPane(
 *   id = "packeta_capable_shipping_information",
 *   label = @Translation("Shipping information compatible with packeta"),
 *   display_label = @Translation("Shipping information"),
 *   wrapper_element = "fieldset",
 * )
 */
class PickupCapableShippingInformation extends ShippingInformation {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, EntityTypeBundleInfo $entity_type_bundle_info, InlineFormManager $inline_form_manager, PackerManagerInterface $packer_manager, OrderShipmentSummaryInterface $order_shipment_summary) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager, $entity_type_bundle_info, $inline_form_manager, $packer_manager, $order_shipment_summary);
    // Set requirements for shipping profile as false in order for our logic to work properly
    $this->configuration['require_shipping_profile'] = FALSE;
    $this->configuration['auto_recalculate'] = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Sadly this functions code had to be copy pasted with only few replacements.
   *
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $store = $this->order->getStore();
    $available_countries = [];
    foreach ($store->get('shipping_countries') as $country_item) {
      $available_countries[] = $country_item->value;
    }

    // Pickup custom: select inline form.
    if (!self::isPickupSelected($pane_form, $form_state, $this->order)) {
      /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
      $inline_form = $this->inlineFormManager->createInstance('customer_profile', [
        'profile_scope' => 'shipping',
        'available_countries' => $available_countries,
        'address_book_uid' => $this->order->getCustomerId(),
        // Don't copy the profile to address book until the order is placed.
        'copy_on_save' => FALSE,
      ], $this->getShippingProfilePickup());
    }
    else {
      $shipping_method = $this->getShippingMethod($pane_form, $form_state, $this->order);

      $inline_form = $this->inlineFormManager->createInstance('pickup_profile', [
        'profile_scope' => 'shipping',
        'available_countries' => $available_countries,
        'shipping_method' => $shipping_method,
      ], $this->getShippingProfilePickup(TRUE));
    }

    // Prepare the form for ajax.
    // Not using Html::getUniqueId() on the wrapper ID to avoid #2675688.
    $pane_form['#wrapper_id'] = 'shipping-information-wrapper';
    $pane_form['#prefix'] = '<div id="' . $pane_form['#wrapper_id'] . '">';
    $pane_form['#suffix'] = '</div>';
    // Auto recalculation is enabled only when a shipping profile is required.
    $pane_form['#auto_recalculate'] = !empty($this->configuration['auto_recalculate']) && !empty($this->configuration['require_shipping_profile']);
    $pane_form['#after_build'][] = [static::class, 'autoRecalculateProcess'];

    $pane_form['shipping_profile'] = [
      '#parents' => array_merge($pane_form['#parents'], ['shipping_profile']),
      '#inline_form' => $inline_form,
    ];
    $pane_form['shipping_profile'] = $inline_form->buildInlineForm($pane_form['shipping_profile'], $form_state);
    $triggering_element = $form_state->getTriggeringElement();
    // The shipping_profile should always exist in form state (and not just
    // after "Recalculate shipping" is clicked).
    if (!$form_state->has('shipping_profile') ||
      // For some reason, when the address selected is changed, the shipping
      // profile in form state is stale.
      (isset($triggering_element['#parents']) && in_array('select_address', $triggering_element['#parents'], TRUE))) {
      $form_state->set('shipping_profile', $inline_form->getEntity());
    }

    $class = get_class($this);
    // Ensure selecting a different address refreshes the entire form.
    if (isset($pane_form['shipping_profile']['select_address'])) {
      $pane_form['shipping_profile']['select_address']['#ajax'] = [
        'callback' => [$class, 'ajaxRefreshForm'],
        'element' => $pane_form['#parents'],
      ];
      // Selecting a different address should trigger a recalculation.
      $pane_form['shipping_profile']['select_address']['#recalculate'] = TRUE;
    }

    $pane_form['recalculate_shipping'] = [
      '#type' => 'button',
      '#value' => $this->t('Recalculate shipping'),
      '#recalculate' => TRUE,
      '#ajax' => [
        'callback' => [$class, 'ajaxRefreshForm'],
        'element' => $pane_form['#parents'],
      ],
      // The calculation process only needs a valid shipping profile.
      '#limit_validation_errors' => [
        array_merge($pane_form['#parents'], ['shipping_profile']),
      ],
      '#after_build' => [
        [static::class, 'clearValues'],
      ],
    ];
    $pane_form['removed_shipments'] = [
      '#type' => 'value',
      '#value' => [],
    ];
    $pane_form['shipments'] = [
      '#type' => 'container',
    ];

    $shipping_profile = $form_state->get('shipping_profile');
    $shipments = $this->order->get('shipments')->referencedEntities();
    $recalculate_shipping = $form_state->get('recalculate_shipping');
    $can_calculate_rates = $this->canCalculateRates($shipping_profile);

    // If the shipping recalculation is triggered, ensure the rates can
    // be recalculated (i.e a valid address is entered).
    if ($recalculate_shipping && !$can_calculate_rates) {
      $recalculate_shipping = FALSE;
      $shipments = [];
    }

    // Ensure the profile is saved with the latest address, it's necessary
    // to do that in case the profile isn't new, otherwise the shipping profile
    // referenced by the shipment won't reflect the updated address.
    if (!$shipping_profile->isNew() &&
      $shipping_profile->hasTranslationChanges() &&
      $can_calculate_rates) {
      $shipping_profile->save();
      $inline_form->setEntity($shipping_profile);
    }

    $force_packing = empty($shipments) && $can_calculate_rates;
    if ($recalculate_shipping || $force_packing) {
      // We're still relying on the packer manager for packing the order since
      // we don't want the shipments to be saved for performance reasons.
      // The shipments are saved on pane submission.
      [$shipments, $removed_shipments] = $this->packerManager->packToShipments($this->order, $shipping_profile, $shipments);

      // Store the IDs of removed shipments for submitPaneForm().
      $pane_form['removed_shipments']['#value'] = array_map(function ($shipment) {
        /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
        return $shipment->id();
      }, $removed_shipments);
    }

    $single_shipment = count($shipments) === 1;
    foreach ($shipments as $index => $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $pane_form['shipments'][$index] = [
        '#parents' => array_merge($pane_form['#parents'], ['shipments', $index]),
        '#array_parents' => array_merge($pane_form['#parents'], ['shipments', $index]),
        '#type' => $single_shipment ? 'container' : 'fieldset',
        '#title' => $shipment->getTitle(),
      ];
      $form_display = EntityFormDisplay::collectRenderDisplay($shipment, 'checkout');
      $form_display->removeComponent('shipping_profile');
      $form_display->buildForm($shipment, $pane_form['shipments'][$index], $form_state);
      $pane_form['shipments'][$index]['#shipment'] = $shipment;

      // Pickup custom: Add ajax.
      $widget = &$pane_form['shipments'][$index]['shipping_method']['widget'][0];

      $widget['#ajax'] = [
        'callback' => [self::class, 'ajaxRefreshForm'],
        'element' => $widget['#field_parents'],
      ];
      $widget['#limit_validation_errors'] = [
        $widget['#field_parents'],
      ];
    }

    // Update the shipments and save the order if no rate was explicitly
    // selected, that usually occurs when changing addresses, this will ensure
    // the default rate is selected/applied.
    if (!$this->hasRateSelected($pane_form, $form_state) && ($recalculate_shipping || $force_packing)) {
      array_map(function (ShipmentInterface $shipment) {
        if (!$shipment->isNew()) {
          $shipment->save();
        }
      }, $shipments);
      $this->order->set('shipments', $shipments);
      $this->order->save();
    }

    // Pickup custom: place at top.
    $pane_form['shipments']['#weight'] = -999;

    unset($pane_form['recalculate_shipping']);

    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $shipping_method = NestedArray::getValue(
      $form_state->getValues(),
      array_merge($pane_form['#parents'], ['shipments', 0, 'shipping_method', 0])
    );

    $is_packeta = strpos($shipping_method, 'packeta') !== FALSE;

    $profile_data = NestedArray::getValue(
      $form_state->getValues(),
      array_merge($pane_form['#parents'], ['shipping_profile'])
    );

    $is_packeta_profile = isset($profile_data['packeta']);

    // Check if chosen shipping method and profile are matching
    if ($is_packeta !== $is_packeta_profile) {
      $form_state->setError($pane_form, $this->t('Illegal input, refresh the page before continuing.'));
    }

    parent::validatePaneForm($pane_form, $form_state, $complete_form);
  }

  /**
   * Determines if a pickup shipping method is selected.
   *
   * This function is static, to not duplicate code.
   * Also used in ProfileFieldCopyWithoutPickup.
   *
   * @param array|NULL $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return bool
   */
  public static function isPickupSelected(?array $form, FormStateInterface $form_state, OrderInterface $order): bool {

    if ($form != NULL) {
      $shipping_method = NestedArray::getValue(
        $form_state->getUserInput(),
        array_merge($form['#parents'], ['shipments', 0, 'shipping_method', 0])
      );
    }

    if (empty($shipping_method)) {
      return self::isShipmentPickup($order);
    }

    return strpos($shipping_method, 'packeta') !== FALSE;
  }

  /**
   * Determine whether shipment shipping method is pickup
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface|null $order
   *
   * @return bool
   */
  public static function isShipmentPickup(?OrderInterface $order): bool {
    if ($order == NULL) {
      return FALSE;
    }

    /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
    $shipments = $order->get('shipments')->referencedEntities();
    $shipment = reset($shipments);

    return $shipment !== FALSE &&
    !empty($shipment->getShippingMethod()) &&
      $shipment->getShippingMethod()
        ->getPlugin()
        ->getPluginId() === 'packeta';
  }


  /**
   * Determines current shipping method selected
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *
   * @return \Drupal\commerce_shipping\Entity\ShippingMethodInterface|\Drupal\Core\Entity\EntityInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getShippingMethod(array $form, FormStateInterface $form_state, OrderInterface $order) {
    $shipping_method = NestedArray::getValue(
      $form_state->getUserInput(),
      array_merge($form['#parents'], ['shipments', 0, 'shipping_method', 0])
    );

    if (empty($shipping_method)) {

      /** @var \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment */
      $shipments = $order->get('shipments')->referencedEntities();
      $shipment = reset($shipments);

      $shipping_method = $shipment->getShippingMethod();

      return $shipping_method;
    }

    $arr = explode('--', $shipping_method);
    $shipping_method_id = reset($arr);

    $shipping_method = $this->entityTypeManager
      ->getStorage('commerce_shipping_method')->load($shipping_method_id);

    return $shipping_method;
  }

  /**
   * Gets the shipping profile and clears address data on switch.
   *
   * @param bool $is_current_pickup
   *   Is pickup store profile.
   *
   * @return \Drupal\profile\Entity\ProfileInterface
   *   The retrieved profile.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getShippingProfilePickup(bool $is_current_pickup = FALSE): ProfileInterface {
    $profile = $this->getShippingProfile();

    $this->setLastShippingProfile($profile);

    $is_last_pickup = $profile->getData('is_pickup_point', FALSE);

    // Swap pickup and generic profile on shipping method change
    if ($is_current_pickup !== $is_last_pickup) {
      $last_profile = $this->getLastShippingProfile($is_current_pickup);

      if ($last_profile != NULL) {
        return $last_profile;
      }

      $profile = $this->entityTypeManager->getStorage('profile')->create([
        'type' => $profile->bundle(),
        'uid' => 0,
      ]);

      $profile->setData('is_pickup_point', $is_current_pickup);

      $this->order->save();
    }

    return $profile;
  }

  /**
   * @param \Drupal\profile\Entity\ProfileInterface $profile
   *
   * @return void
   */
  protected function setLastShippingProfile(ProfileInterface $profile) {
    if ($profile->id() != NULL) {
      if ($profile->getData('is_pickup_point', FALSE)) {
        $this->order->setData('shipping_profile.last_packeta', $profile->id());
      } else {
        $this->order->setData('shipping_profile.last_generic', $profile->id());
      }
    }
  }

  /**
   * @param bool $isPickup
   *
   * @return \Drupal\profile\Entity\ProfileInterface|null
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getLastShippingProfile(bool $isPickup = FALSE): ?ProfileInterface {
    if ($isPickup) {
      $profile_id = $this->order->getData('shipping_profile.last_packeta');
    } else {
      $profile_id = $this->order->getData('shipping_profile.last_generic');
    }

    if ($profile_id != NULL) {
      return $this->entityTypeManager->getStorage('profile')->load($profile_id);
    }

    return NULL;
  }

}
