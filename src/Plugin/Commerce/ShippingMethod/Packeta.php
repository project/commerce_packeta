<?php


namespace Drupal\commerce_packeta\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Packeta shipping method.
 *
 * @CommerceShippingMethod(
 *  id = "packeta",
 *  label = @Translation("Packeta")
 * )
 */
class Packeta extends ShippingMethodBase implements SupportsTrackingInterface {

  protected $languageManager;

  protected $entityFieldManager;

  protected $moduleHandler;

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('language_manager'),
      $container->get('entity_field.manager'),
      $container->get('module_handler')
    );
  }

  /**
   * List of all currently available countries for Packeta
   */
  public const AVAILABLE_COUNTRIES = [
    'cz' => 'Czech Republic',
    'sk' => 'Slovakia',
    'de' => 'Germany',
    'hu' => 'Hungary',
    'pl' => 'Poland',
    'at' => 'Austria',
    'ro' => 'Romania',
    'bg' => 'Bulgaria',
  ];

  /**
   * Constructs a new Packeta object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              PackageTypeManagerInterface $package_type_manager,
                              WorkflowManagerInterface $workflow_manager,
                              LanguageManagerInterface $languageManager,
                              EntityFieldManagerInterface $entityFieldManager,
                              ModuleHandlerInterface $moduleHandler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->languageManager = $languageManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->moduleHandler = $moduleHandler;
    $this->services['default'] = new ShippingService('packeta', $this->configuration['rate_label']);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
        'rate_label' => '',
        'rate_amount' => NULL,
        'settings' => [
          'eshop_url' => '',
          'cash_on_delivery' => FALSE,
          'allowed_countries' => [],
          'telephone_field' => '',
        ],
        'api' => [
          'api_key' => '',
          'api_password' => '',
        ],
        'widget' => [
          'widget_font' => 'Arial',
          'widget_selector_open' => '',
        ],
        'services' => ['default'],
        'tracking_url' => 'https://tracking.packeta.com/[langcode]/?id=[tracking_code]',
      ] + parent::defaultConfiguration();
  }

  /**
   * Returns all fonts available for use in Packeta widget
   *
   * @return string[]
   */
  public function availableFonts(): array {
    return [
      'Arial' => 'Arial',
      'Open Sans' => 'Open Sans',
      'Times New Roman' => 'Times New Roman',
      'Verdana' => 'Verdana',
      'Trebuchet MS' => 'Trebuchet MS',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $amount = $this->configuration['rate_amount'];
    // A bug in the plugin_select form element causes $amount to be incomplete.
    if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
      $amount = NULL;
    }

    $this->messenger()->addWarning(
      $this->t(
        'Since 1. September 2021 Packeta API requires weight for package submission.
          Make sure to use weight in all products for automatic package submission when order gets accepted.
          Otherwise, package submission will fail and must be done manually.'
      )
    );
    $this->messenger()->addWarning(
      $this->t(
        'We recommend to set shipment condition for weight to be higher than 0 kg.'
      )
    );

    $form['api'] = [
      '#type' => 'container',
      '#title' => $this->t('API Settings'),
      'api_key' => [
        '#type' => 'textfield',
        '#title' => $this->t('API key'),
        '#description' => $this->t('API key provided by Packeta.'),
        '#default_value' => $this->configuration['api']['api_key'],
        '#required' => TRUE,
      ],
      'api_password' => [
        '#type' => 'textfield',
        '#title' => $this->t('API password'),
        '#description' => $this->t('API password provided by Packeta.'),
        '#default_value' => $this->configuration['api']['api_password'],
        '#required' => TRUE,
      ],
    ];

    $form['rate_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate label'),
      '#description' => $this->t('Shown to customers when selecting the rate.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => TRUE,
    ];

    $form['rate_amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Rate amount'),
      '#default_value' => $amount,
      '#required' => TRUE,
    ];

    $form['settings'] = [
      '#type' => 'container',
      '#title' => $this->t('Settings'),
      'eshop_url' => [
        '#type' => 'textfield',
        '#title' => $this->t('E-shop URL Address'),
        '#default_value' => $this->configuration['settings']['eshop_url'],
        '#required' => TRUE,
      ],
      'cash_on_delivery' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Cash on delivery'),
        '#description' => $this->t('If checked, all packages will require payment on delivery'),
        '#default_value' => $this->configuration['settings']['cash_on_delivery'],
      ],
      'allowed_countries' => [
        '#type' => 'checkboxes',
        '#title' => $this->t('Allowed countries'),
        '#description' => $this->t('When no countries are selected, all of them are allowed'),
        '#default_value' => $this->configuration['settings']['allowed_countries'],
        '#options' => self::AVAILABLE_COUNTRIES,
      ],
    ];

    $form['widget'] = [
      '#type' => 'container',
      '#title' => $this->t('Widget settings'),
      'widget_font' => [
        '#type' => 'select',
        '#title' => $this->t('Widget font'),
        '#default_value' => $this->configuration['widget']['widget_font'],
        '#options' => $this->availableFonts(),
      ],
      'widget_selector_open' => [
        '#type' => 'textfield',
        '#title' => $this->t('CSS classes of widget button'),
        '#description' => $this->t('These css classes will be applied to button used to open packeta widget example: (.example1 .example2)'),
        '#default_value' => $this->configuration['widget']['widget_selector_open'],
      ],
    ];

    $is_telephone_enabled = $this->moduleHandler->moduleExists('telephone');

    if ($is_telephone_enabled) {
      $telephone_field_options = [
        '' => $this->t(' - None - '),
      ];

      // Retrieve all telephone type fields from all profile bundles
      $fields = $this->entityFieldManager->getFieldStorageDefinitions('profile');
      foreach ($fields as $field_name => $field) {
        if ($field->getType() == 'telephone') {
          $telephone_field_options[$field_name] = $field->getName();
        }
      }

      $form['settings']['telephone_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Telephone field'),
        '#description' => $this->t('If there is a telephone field you want to map into Packeta API, choose it here'),
        '#default_value' => $this->configuration['settings']['telephone_field'],
        '#options' => $telephone_field_options,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['rate_label'] = $values['rate_label'];
      $this->configuration['rate_amount'] = $values['rate_amount'];

      $this->configuration['settings']['eshop_url'] = $values['settings']['eshop_url'];
      $this->configuration['settings']['cash_on_delivery'] = $values['settings']['cash_on_delivery'];
      $this->configuration['settings']['allowed_countries'] = $values['settings']['allowed_countries'];

      if (isset($values['settings']['telephone_field'])) {
        $this->configuration['settings']['telephone_field'] = $values['settings']['telephone_field'];
      }

      $this->configuration['api']['api_key'] = $values['api']['api_key'];
      $this->configuration['api']['api_password'] = $values['api']['api_password'];

      $this->configuration['widget']['widget_font'] = $values['widget']['widget_font'];
      $this->configuration['widget']['widget_selector_open'] = $values['widget']['widget_selector_open'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment): array {
    $rates = [];
    $rates[] = new ShippingRate([
      'shipping_method_id' => $this->parentEntity->id(),
      'service' => $this->services['default'],
      'amount' => Price::fromArray($this->configuration['rate_amount']),
    ]);

    return $rates;
  }

  /**
   * Returns a tracking URL for Packeta shipments.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The commerce shipment.
   *
   * @return \Drupal\core\Url|false
   *   The URL object or FALSE.
   */
  public function getTrackingUrl(ShipmentInterface $shipment) {
    $code = $shipment->getTrackingCode();
    $langcode = $this->languageManager->getCurrentLanguage()->getId();

    if (!empty($code)) {
      $url = $this->configuration['tracking_url'];

      $url = str_replace('[langcode]', $langcode, $url);
      $url = str_replace('[tracking_code]', $code, $url);
      return Url::fromUri($url);
    }

    return FALSE;
  }

}
