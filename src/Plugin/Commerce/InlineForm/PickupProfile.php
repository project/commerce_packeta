<?php

namespace Drupal\commerce_packeta\Plugin\Commerce\InlineForm;

use Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormBase;
use Drupal\commerce_packeta\PickupProfileMapperInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\profile\Entity\ProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an inline form for managing a customer profile.
 *
 * Allows copying values to and from the customer's address book.
 *
 * Supports two modes, based on the profile type setting:
 * - Single: The customer can have only a single profile of this type.
 * - Multiple: The customer can have multiple profiles of this type.
 *
 * @CommerceInlineForm(
 *   id = "pickup_profile",
 *   label = @Translation("Customer profile"),
 * )
 */
final class PickupProfile extends EntityInlineFormBase {

  /**
   * The profile mapper.
   *
   * @var \Drupal\commerce_packeta\PickupProfileMapperInterface
   */
  public $pickupProfileMapper;

  /**
   * Constructs a new CustomerProfile object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_packeta\PickupProfileMapperInterface $pickupProfileMapper
   *   The pickup profile mapper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    PickupProfileMapperInterface $pickupProfileMapper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->pickupProfileMapper = $pickupProfileMapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_packeta.default_profile_mapper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      // Unique. Passed along to field widgets. Examples: 'billing', 'shipping'.
      'profile_scope' => 'shipping',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(array $inline_form, FormStateInterface $form_state) {
    $inline_form = parent::buildInlineForm($inline_form, $form_state);

    assert($this->entity instanceof ProfileInterface);
    assert($this->pickupProfileMapper instanceof PickupProfileMapperInterface);

    $shipping_method = $this->configuration['shipping_method'];

    $this->pickupProfileMapper->setShippingMethod($shipping_method);

    $inline_form['packeta'] = $this->pickupProfileMapper->buildFormElement($this->entity);

    return $inline_form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitInlineForm(array &$inline_form, FormStateInterface $form_state) {
    parent::submitInlineForm($inline_form, $form_state);
    assert($this->pickupProfileMapper instanceof PickupProfileMapperInterface);
    $data = NestedArray::getValue($form_state->getValues(), $inline_form['#parents']);

    $triggering_element = $form_state->getTriggeringElement();

    // Validate pickup point ID only on non-AJAX form submits
    // Triggering element ID is 'edit-action-next' only on non-ajax submits
    if (empty($data['packeta']['selected_point']['id']) && $triggering_element['#id'] == 'edit-actions-next') {
      $form_state->setError($inline_form, $this->t("Pickup point not selected"));
    }

    $point_data = $data['packeta']['selected_point'];

    $this->entity->setData('pickup_point_id', $point_data['id']);
    // Do not save a dealer pickup address into the users address book.
    $this->entity->unsetData('address_book_profile_id');
    $this->entity->unsetData('copy_to_address_book');
    $this->pickupProfileMapper->populateProfile($this->entity, $point_data);
    $this->entity->save();
  }

}
