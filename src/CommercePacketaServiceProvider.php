<?php


namespace Drupal\commerce_packeta;


use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

class CommercePacketaServiceProvider extends ServiceProviderBase {

  public function alter(ContainerBuilder $container) {

    // Override commerce_shipping service
    if ($container->hasDefinition('commerce_shipping.profile_field_copy')) {
      $definition = $container->getDefinition('commerce_shipping.profile_field_copy');
      $definition->setClass('Drupal\commerce_packeta\ProfileFieldCopyWithoutPickup');
    }
  }

}
