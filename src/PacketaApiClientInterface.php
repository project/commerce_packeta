<?php


namespace Drupal\commerce_packeta;


use Drupal\commerce_shipping\Entity\ShipmentInterface;

interface PacketaApiClientInterface {

  const LOGGER_CHANNEL = 'commerce_packeta';

  /**
   * Submits the shipment to Packeta API and retrieves tracking code
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *
   * @return string
   *    Tracking code
   */
  public function submitShipment(ShipmentInterface $shipment): string;

}
